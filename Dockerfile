FROM ubuntu:18.04

RUN apt-get update && \
    apt-get -y install python3 python3-pip python3-mysqldb && \
    rm -rf /var/lib/apt/lists/*
RUN pip3 install --no-cache-dir requests flask flask-jsonpify flask-restful

WORKDIR /usr/local/bin
COPY src/ /usr/local/bin/

EXPOSE 8080
CMD [ "python3", "-u", "./api.py" ]