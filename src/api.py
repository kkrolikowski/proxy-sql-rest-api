#!/usr/bin/python

from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask import jsonify
from flask.views import MethodView
from lib.apiclass import *

app = Flask(__name__)
api = Api(app)


api.add_resource(Test, '/test')
api.add_resource(Accounts, '/accounts')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8080')