from flask import Flask, request
from flask import Response
from flask_restful import Resource, Api
import json
from flask import jsonify
from flask.views import MethodView
import os
import MySQLdb
from lib.database import *

class Test(MethodView):
    def get(self):
        resp = [
            {
                'app_name': u'proxysql-api',
                'status': u'ok'
            }
        ]
        return resp

class Accounts(MethodView):
    def post(self):

        proxyNotReady = {
            "status": "ProxySQL Database not ready",
            "code": "425"
        }
        sqlnodeConnError = {
            "status": "SQL Node connection error",
            "code": "426"
        }
        accountCreated = {
            "status": "Account created",
            "code": "200"
        }
        invalidData = {
            "status": "Invalid input data",
            "code": "400"
        }

        proxyhost = os.environ['SQLPROXY_HOST']
        login = os.environ['PROXY_ADMIN_USER']
        password = os.environ['PROXY_ADMIN_PASS']
        rootpass = os.environ['MYSQL_ROOT_PASSWORD']

        try:
            proxydb = Database(proxyhost, 6032, login, password)
        except:
            js = json.dumps(proxyNotReady)
            return Response(js, status=425, mimetype='application/json')
        else:
            dblogin = request.json['account']['login']
            dbpass = request.json['account']['password']
            database = request.json['account']['database']
            grants = request.json['account']['grants']

            if all(var is not None for var in [dblogin, dbpass, database, grants]):
                if proxydb.addAcoount(dblogin, dbpass, database, grants, rootpass):
                    js = json.dumps(accountCreated)
                    return Response(js, status=200, mimetype='application/json')
                else:
                    js = json.dumps(sqlnodeConnError)
                    return Response(js, status=426, mimetype='application/json')
            else:
                js = json.dumps(invalidData)
                return Response(js, status=400, mimetype='application/json')