import MySQLdb

class Database:
    def __init__(self, host, port, user, passwd):
        self.db = None
        try:
            self.db = MySQLdb.connect(host=host, port=port, user=user, passwd=passwd)
        except MySQLdb.Error:
            return None

    def addAcoount(self, dblogin, dbpass, dbname, grants, rootpass):
        cur = self.db.cursor()
        sqlt = ("INSERT INTO mysql_users (username, password, active, default_hostgroup, max_connections) ",
                    "VALUES ('" + dblogin + "', '" + dbpass + "', 1, 0, 200)")
        sqls = ''.join(sqlt)
        cur.execute(sqls)
        sqls = "SELECT hostname FROM runtime_mysql_servers where status = 'ONLINE' LIMIT 1"
        cur.execute(sqls)
        res = cur.fetchone()
        sqlnode = ''.join(res)

        db = MySQLdb.connect(host=sqlnode, port=3306, user="root", passwd=rootpass)
        if db:
            nodecur = db.cursor()
            sqls = "GRANT ALL ON " + dbname + ".* to '" + dblogin + "'@'%' identified by '" + dbpass + "'"
            nodecur.execute(sqls)

            cur.execute("LOAD MYSQL USERS TO RUNTIME")
            cur.execute("SAVE MYSQL USERS TO DISK")

            return 1
        else:
            return 0
